**This is a  OpenMp code for self-ssembly of tetrahedron in the binary mixture of negative and positive charge colloids with hybrid Monte Carlo in NVT and NPT ensembles, written in C.**

    	 


 *	 Compile  using: 
 *      gcc filename.c -lm 



 *	 Run :
 *		 nohup ./a.out &
 	 			
Initial Configuration

![hmc1](/uploads/2f975d48a05e549648cb4f025ade62f5/hmc1.png)

tetrahedron assembly with NVT hybrid Monte Carlo

![hmc3](/uploads/acb3546531fbe0e2062d24d05d33ea36/hmc3.png)

tetrahedron assembly with NPT hybrid Monte Carlo

![hmc6](/uploads/a36ec4cb22425467b158425e0be3deb3/hmc6.png)

 

 Written By::<br/>
       **Rajneesh Kumar**
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	27 Dec, 2020


